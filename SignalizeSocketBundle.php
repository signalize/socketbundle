<?php

namespace Signalize\SocketBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SignalizeSocketBundle extends Bundle
{
    static public function registerBundle(&$bundles)
    {
        $bundles[] = new self;
    }
}
