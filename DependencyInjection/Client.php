<?php

namespace Signalize\SocketBundle\DependencyInjection;

use Ratchet\ConnectionInterface;
use Signalize\SocketBundle\Model\MessageModelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class Client implements ClientInterface
{
    /**
     * The service container
     * @var ContainerInterface $_container
     */
    private $_container;

    /**
     * The connection
     * @var ConnectionInterface $_connection
     */
    private $_connection;

    /**
     * {@inheritdoc}
     */
    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function create(ConnectionInterface $connection): ClientInterface
    {
        $this->_connection = $connection;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasConnection(ConnectionInterface $connection): bool
    {
        return $this->_connection === $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function maySendTo(ClientInterface $client): bool
    {
        //Check or the client is allowed to receive a message from the sender(this)
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function send(MessageModelInterface $message)
    {
        $this->_connection->send($message);
    }

    /**
     * {@inheritdoc}
     */
    public function handleMessage(MessageModelInterface $response, ClientInterface $sender)
    {
        switch (true) {
            case $sender->hasConnection($this->_connection):
                //Send data back to the sender
                $this->send($response);
                break;
            case $sender->maySendTo($this):
                //Send data to clients who are allowed to receive the data.
                $this->send($response);
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSection(): string
    {
        return "CLIENT";
    }
}