<?php

namespace Signalize\SocketBundle\DependencyInjection;

use Ratchet\ConnectionInterface;
use Signalize\SocketBundle\Model\MessageModelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface ClientInterface
 * @package Signalize\SocketBundle\DependencyInjection
 * @author Maikel ten Voorde <info@matevo.nl>
 */
interface ClientInterface
{
    /**
     * ClientInterface constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container);

    /**
     * This method allows you to create a new client for a connection
     * @param ConnectionInterface $connection
     * @return ClientInterface
     */
    public function create(ConnectionInterface $connection): ClientInterface;

    /**
     * This method allows you to check or this client has a specific connection
     * @param ConnectionInterface $connection
     * @return bool
     */
    public function hasConnection(ConnectionInterface $connection): bool;

    /**
     * This method allows you to handle a message that has been received
     * @param MessageModelInterface $response
     * @param ClientInterface $sender
     * @return mixed
     */
    public function handleMessage(MessageModelInterface $response, ClientInterface $sender);

    /**
     * This method allows you to check or the client may receive a message from this sender
     * @param ClientInterface $client
     * @return bool
     */
    public function maySendTo(ClientInterface $client): bool;

    /**
     * This method allows you to send a message to this client
     * @param MessageModelInterface $message
     * @return mixed
     */
    public function send(MessageModelInterface $message);

    /**
     * This method allows you to determine within which section this client belongs to displaying information on the command line.
     * @return string
     */
    public function getSection(): string;

}
