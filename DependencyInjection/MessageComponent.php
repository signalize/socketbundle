<?php

namespace Signalize\SocketBundle\DependencyInjection;


use Ratchet\ConnectionInterface;
use Signalize\SocketBundle\Command\CreateCommandInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class MessageComponent implements MessageComponentInterface
{
    /**
     * The container
     * @var ContainerInterface
     */
    private $_container;
    /**
     * The command line interface
     * @var CreateCommandInterface $_console
     */
    private $_console;
    /**
     * All client who are connected to the socket
     * @var \SplObjectStorage
     */
    private $_clients;

    /**
     * {@inheritdoc}
     */
    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
        $this->_clients = new \SplObjectStorage; // @todo: Save this to a storage system?
    }

    /**
     * {@inheritdoc}
     */
    public function load(CreateCommandInterface $console): self
    {
        $this->_console = $console;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    function onOpen(ConnectionInterface $conn)
    {
        $client = $this->_container->get('signalize_socket.client')->create($conn);
        $this->_clients->attach($client);
        $this->printMessage('New connection opened.', 'green', $client->getSection());
    }

    /**
     * {@inheritdoc}
     */
    function onMessage(ConnectionInterface $from, $msg)
    {
        $request = $this->_container->get('signalize_socket.request')->createFromMessage($msg);
        if (!$client = $this->getClient($from)) {
            throw new \Exception('Unidentifiable client!');
        }

        if ($response = $request->execute()) {
            /** @var ClientInterface $c */
            foreach ($this->_clients as $c) {
                $c->handleMessage($response, $client);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    function onClose(ConnectionInterface $conn)
    {
        /** @var Client $client */
        foreach ($this->_clients as $client) {
            if ($client->hasConnection($conn)) {
                $this->_clients->detach($client);
                $this->printMessage('Connection closed.', 'yellow', $client->getSection());
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->printMessage($e->getMessage(), 'red', 'ERROR');
    }

    /**
     * This method allows you to find the client who belongs to a connection
     * @param ConnectionInterface $conn
     * @return Client
     * @throws \Exception
     */
    private function getClient(ConnectionInterface $conn)
    {
        /** @var Client $client */
        foreach ($this->_clients as $client) {
            if ($client->hasConnection($conn)) {
                return $client;
            }
        }
        throw new \Exception('Client cannot be found!');
    }

    /**
     * This method allows you to print a message to the command line
     * @param string|null $msg
     * @param string|null $color
     * @param string|null $section
     */
    private function printMessage(string $msg = null, string $color = null, string $section = null)
    {
        if ($this->_console) {
            $this->_console->printMessage($msg, $color, $section);
        }
    }
}

