<?php

namespace Signalize\SocketBundle\DependencyInjection;


use Signalize\SocketBundle\Command\CreateCommandInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface MessageComponentInterface
 * @package Signalize\SocketBundle\DependencyInjection
 * @author Maikel ten Voorde <info@matevo.nl>
 */
interface MessageComponentInterface extends \Ratchet\MessageComponentInterface
{
    /**
     * MessageComponentInterface constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container);

    /**
     * Load the command line interface
     * @param CreateCommandInterface $console
     * @return MessageComponent
     */
    public function load(CreateCommandInterface $console): MessageComponent;


}
