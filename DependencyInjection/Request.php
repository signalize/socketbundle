<?php

namespace Signalize\SocketBundle\DependencyInjection;

use Signalize\SocketBundle\Model\MessageModelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * {@inheritdoc}
 */
class Request implements RequestInterface
{
    /**
     * Service Container
     * @var ContainerInterface $_container
     * */
    private $_container;
    /**
     * The message that will be used for the request
     * @var MessageModelInterface $_message
     */
    private $_message;
    /**
     * The request that will be executed
     * @var \Symfony\Component\HttpFoundation\Request $_request
     */
    private $_request;

    /**
     * {@inheritdoc}
     */
    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromMessage(string $msg): RequestInterface
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        /** @var MessageModelInterface $msg */
        $this->_message = $serializer->deserialize($msg, $this->_container->getParameter('signalize_socket.model_class'), 'json');

        //Setup the path
        if (!$this->_message->hasPath()) {
            throw new \Exception('Cannot execute the received message! No path found!', Response::HTTP_BAD_REQUEST);
        }

        //Create request
        $this->_request = \Symfony\Component\HttpFoundation\Request::create($this->_message->getPath(), $this->_message->getMethod());

        //Add headers
        foreach ($this->_message->getHeaders() as $header => $value) {
            if (!is_numeric($header)) {
                $this->_request->headers->set($header, $value);
            }
        }

        //Add postdata
        $this->_request->request->add($this->_message->getData());

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get(): \Symfony\Component\HttpFoundation\Request
    {
        return $this->_request;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(): MessageModelInterface
    {
        $response = $this->_container->get('http_kernel')->handle($this->_request, HttpKernelInterface::MASTER_REQUEST)->getContent();
        $this->_message->setResponse((new JsonDecode())->decode($response, 'json'));
        return $this->_message;
    }
}