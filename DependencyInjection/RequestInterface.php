<?php

namespace Signalize\SocketBundle\DependencyInjection;

use Signalize\SocketBundle\Model\MessageModelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface RequestInterface
 * @package Signalize\SocketBundle\DependencyInjection
 * @author Maikel ten Voorde <info@matevo.nl>
 */
interface RequestInterface
{
    /**
     * RequestInterface constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container);

    /**
     * This method allows you to create the message and the request from a json encoded string
     * @param string $msg
     * @return RequestInterface
     */
    public function createFromMessage(string $msg): RequestInterface;

    /**
     * This method allows you to get the request
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function get(): \Symfony\Component\HttpFoundation\Request;

    /**
     * This method allows you to execute the request
     * @return MessageModelInterface
     */
    public function execute(): MessageModelInterface;
}