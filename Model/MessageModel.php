<?php

namespace Signalize\SocketBundle\Model;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncode;

/**
 * {@inheritdoc}
 */
class MessageModel implements MessageModelInterface
{
    public $path = '';
    public $method = 'GET';
    public $data = [];
    public $headers = [];
    public $response = null;

    /**
     * {@inheritdoc}
     */
    public function hasPath(): bool
    {
        return !empty($this->path);
    }

    /**
     * {@inheritdoc}
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function setPath(string $path)
    {
        if (is_string($path) && !empty($path)) {
            $this->path = $path;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isMethod(string $method): bool
    {
        return $this->method == $method;
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * {@inheritdoc}
     */
    public function setMethod(string $method)
    {
        if (!in_array($method, ['GET', 'POST', 'PATCH', 'DELETE', 'PUT'])) {
            throw new \Exception('Invalid method!', Response::HTTP_METHOD_NOT_ALLOWED);
        }
        $this->method = $method;
    }

    /**
     * {@inheritdoc}
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * {@inheritdoc}
     */
    public function setData($data)
    {
        if (is_array($data)) {
            $this->data = $data;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * {@inheritdoc}
     */
    public function setHeaders($headers)
    {
        if (is_array($headers)) {
            $this->headers = $headers;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * {@inheritdoc}
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return (new JsonEncode())->encode($this, 'json');
    }


}