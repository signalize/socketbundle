<?php

namespace Signalize\SocketBundle\Model;

/**
 * Interface MessageModelInterface
 * @package Signalize\SocketBundle\Model
 * @author Maikel ten Voorde <info@matevo.nl>
 */
interface MessageModelInterface
{
    /**
     * Check or the message has a path configured
     * @return bool
     */
    public function hasPath(): bool;

    /**
     * Get the path of this message
     * @return string
     */
    public function getPath(): string;

    /**
     * Set the path of this message
     * @param string $path
     */
    public function setPath(string $path);

    /**
     * Check or the message has a specific method
     * @param string $method
     * @return bool
     */
    public function isMethod(string $method): bool;

    /**
     * Get the method of this message
     * @return string
     */
    public function getMethod(): string;

    /**
     * Set the method of this message
     * @param string $method
     * @throws \Exception
     */
    public function setMethod(string $method);

    /**
     * Get the data of this message
     * @return array
     */
    public function getData(): array;

    /**
     * Set the data of this message
     * @param array $data
     */
    public function setData($data);

    /**
     * Get the headers of this message
     * @return array
     */
    public function getHeaders(): array;

    /**
     * Set the headers of this message
     * @param array $headers
     */
    public function setHeaders($headers);

    /**
     * Get the response of this message
     * @return string|object|array
     */
    public function getResponse();

    /**
     * Set the response of this message
     * @param string|object|array $response
     */
    public function setResponse($response);

    /**
     * Return a json encoded string of this message
     * @return string
     */
    public function __toString(): string;


}