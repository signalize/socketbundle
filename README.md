# Signalize Socket Bundle

A PHP library for asynchronously serving WebSockets.
Through this socket server you can call local routes. 
The result of these route requests can then be returned to multiple connected clients.

## Installation

Add the repository to your composer file
```
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:signalize/socketbundle.git"
        }
    ]
```

Add a require to your composer file
```
"signalize/socketbundle": "dev-master"
```

Update composer
``` 
$ composer update
```

Add this bundle in your application kernel

```
// app/AppKernel.php
public function registerBundles()
{
    // ...
    Signalize\SocketBundle\SignalizeSocketBundle::registerBundle($bundles);

    return $bundles;
}
```
    
## Usage

### Extend functionality
You can configure the operation of the socket server as you wish. 
It is possible to access different layers of functionality. 
This can be done through a number of configuration options.

#### The Client 
The client is the user who is connected to the socket. 
This user can be identified based on the data received. 
The client service allows you to save received data so that it can be used later to determine who the user is. 
Then you can decide who can receive messages from the user and who can send messages to this user.

To add an extension to the basic functionality, you can register the service. You can do this by overwriting the service class in your services.yml with the following code:

```
services:
...
    signalize_socket.client:
        class: YOUR_OWN_CLASS
        arguments: ["@service_container"]
```

Attention! Your own class must always extend from the original class. You can find this at the next location:
``` 
Signalize\SocketBundle\DependencyInjection\Client 
```

#### The Request
The request class determines what happens to a message received by the socket. 
This class converts the received message into a request, which will then be executed.

To add an extension to the basic functionality, you can register the service. You can do this by overwriting the service class in your services.yml with the following code:
```
services:
...
    signalize_socket.request:
        class: YOUR_OWN_CLASS
        arguments: ["@service_container"]
```
Attention! Your own class must always extend from the original class. You can find this at the next location:
``` 
Signalize\SocketBundle\DependencyInjection\Request 
```

#### The Message Component
The message component class determines what happens when a new connection to the socket server occurs, a new message is received, or when a connection decides to stop.

To add an extension to the basic functionality, you can register the service. You can do this by overwriting the service class in your services.yml with the following code:
```
services:
...
    signalize_socket.message_component:
        class: YOUR_OWN_CLASS
        arguments: ["@service_container"]
```
Attention! Your own class must always extend from the original class. You can find this at the next location:
``` 
Signalize\SocketBundle\DependencyInjection\MessageComponent 
```

#### The Message Model
The message model determines which message is sent and received directly over the socket connection. It is a fixed structure for sending and receiving data.

To add an extension to the basic functionality, you can register your own class for this model. You do this by configuring the model class in your config.yml.

```
signalize_socket:
    model_class:  YOUR_OWN_CLASS
```
Attention! Your own class must always extend from the original class. You can find this at the next location:
```
Signalize\SocketBundle\Model\MessageModel 
```



### Run the socket

```
php bin/console socket:create --port [PORT] --address [IPADDRESS] 
```