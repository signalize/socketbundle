<?php

namespace Signalize\SocketBundle\Command;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This class will create a new console command.
 *
 * @package Signalize\SocketBundle\Command
 * @author Maikel ten Voorde <info@matevo.nl>
 */
class CreateCommand extends ContainerAwareCommand implements CreateCommandInterface
{
    /**
     * The input message buffer
     * @var InputInterface $_input
     */
    private $_input;
    /**
     * The output message buffer
     * @var OutputInterface $_output
     */
    private $_output;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('socket:create')
            ->setDescription('Creates a new socket server')
            ->setHelp('This command allows you to create a socket server')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('port', 'p', InputOption::VALUE_REQUIRED, 'The port of the socket server'),
                    new InputOption('address', 'a', InputOption::VALUE_REQUIRED, 'The address of the socket server')
                ]));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_input = $input;
        $this->_output = $output;

        $port = ($input->getOption('port') ?: 8080);
        $address = ($input->getOption('address') ?: '0.0.0.0');
        $output->writeln('Start Socket Server on <fg=green;options=bold>' . $address . ':' . $port . '</>');
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $this->getContainer()->get('signalize_socket.message_component')->load($this)
                )
            ),
            $port,
            $address
        );
        $server->run();
    }

    /**
     * {@inheritdoc}
     */
    public function getInput(): InputInterface
    {
        return $this->_input;
    }

    /**
     * {@inheritdoc}
     */
    public function getOutput(): OutputInterface
    {
        return $this->_output;
    }

    /**
     * {@inheritdoc}
     */
    public function printMessage(string $msg, string $color, string $section = null)
    {
        /** @var FormatterHelper $formatter */
        $formatter = $this->getHelper('formatter');

        if (!is_null($section)) {
            $formattedLine = $formatter->formatSection(
                $section,
                '<fg=' . $color . ';options=bold>' . substr($msg, 0, 100) . (strlen($msg) > 100 ? '...' : '') . '</>'
            );
        } else {
            $formattedLine = '<fg=' . $color . ';options=bold>' . $msg . '</>';
        }

        $this->getOutput()->writeln($formattedLine);
    }
}