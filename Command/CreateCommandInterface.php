<?php

namespace Signalize\SocketBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Interface CreateCommandInterface
 * @package Signalize\SocketBundle\Command
 * @author Maikel ten Voorde <info@matevo.nl>
 */
interface CreateCommandInterface
{
    /**
     * Get the input buffer
     * @return InputInterface
     */
    public function getInput(): InputInterface;

    /**
     * Get the output buffer
     * @return OutputInterface
     */
    public function getOutput(): OutputInterface;

    /**
     * Print a message in the console
     *
     * @param string $msg
     * @param string $color
     * @param string $section
     */
    public function printMessage(string $msg, string $color, string $section = null);
}